import React, { useState } from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import {Event} from './data/Event';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import { IonItem, IonLabel, IonToggle } from '@ionic/react';

const localizer = momentLocalizer(moment);

interface props {
    listEvents: Event[];
  }

const Calendrier = ({listEvents }: props) => {

  const [rappelDisabled, setRappelDisabled] = useState(false);

  return (
    <div>
      <IonItem>
        <IonToggle onClick={() => setRappelDisabled(!rappelDisabled)} />
        <IonLabel>Afficher les rappels</IonLabel>
      </IonItem>

      <Calendar
        localizer={localizer}
        events={listEvents}
        startAccessor="start"
        endAccessor="end"
        view='week'
      />
    </div>
  )
};

export default Calendrier;

