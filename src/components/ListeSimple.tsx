import React, { useState } from "react";
import { Data } from "./data/Data";
import {
  IonList,
  IonItem,
  IonLabel,
  IonText,
  IonAlert,
  IonFab,
  IonFabButton,
  IonIcon,
  IonRouterOutlet,
  IonDatetime,
} from "@ionic/react";
import VueDetail from "./VueDetail";
import { addOutline } from "ionicons/icons";
import NewElement from "./NewElement";
import { Redirect, Route } from "react-router-dom";
import NewElementButton from "./NewElementButton";

interface Props {
  list: Data[];
}

const ListeSimple = ({ list }: Props) => {

  const [noteCourante, setNoteCourante] = useState<Data>();
  const [supprimer, setSupprimer] = useState(false);

  return (
    <>
      {list ? (
        <>
          
          <IonList>
            {list.map((element) => (
              <IonItem button onClick={() => setNoteCourante(element)}>
                <IonLabel className="ion-text-wrap">
                  <IonText color="dark">
                    {element.titre}
                  </IonText>
                  <IonText color="medium">
                    <p>{element.description}</p>
                  </IonText>
                </IonLabel>
              </IonItem>
            ))}
          </IonList>

          <IonAlert
            isOpen={noteCourante !== undefined}
            onDidDismiss={() => setNoteCourante(undefined)}
            header={noteCourante?.titre}
            subHeader={noteCourante?.date}
            message={noteCourante?.description}
            buttons={[
              {
                text: "Supprimer",
                cssClass: "secondary",
                handler: () => {
                  console.log("Confirm Suppr");
                  setSupprimer(true);
                  
                },
              },
              {
                text: "Modifier",
                cssClass: "secondary",
                handler: () => {
                  console.log("Confirm Modif");
                },
              },
              {
                text: "Retour",
                role: "cancel",
                cssClass: "secondary",
                handler: () => {
                  console.log("Confirm Cancel");
                },
              }
            ]}
          />

          <IonAlert
            isOpen={supprimer}
            onDidDismiss={() => setSupprimer(false)}
            message={"Voulez-vous vraiment supprimer cette note ?"}
            buttons={[
              {
                text: "Non",
                role: "cancel",
                cssClass: "secondary",
                handler: () => {
                  console.log("Confirm Cancel");
                }
              },
              {
                text: "Oui",
                role: "cancel",
                cssClass: "secondary",
                handler: () => {
                  console.log("Confirm Suppr");
                  
                }
              }
            ]}
          />
           

          <IonFab vertical="bottom" horizontal="end" slot="fixed">
            <IonFabButton color="danger" href="/newelement" >
              <IonIcon icon={addOutline} />
            </IonFabButton>
          </IonFab>
        </>
      ) : (
        <div>Chargement en cours</div>
      )}
    </>
  );
};

export default ListeSimple;

/*
delete list.noteCourante;
*/

/*
                  <IonDatetime
                    value={element.date}
                    displayFormat="MMMM DD, YYYY"
                    pickerFormat="none" />
*/

/*

<IonItem button onClick={() =>{
                setShowElement(true)
              }
              }>

<IonAlert 
                  isOpen={showElement}
                  onDidDismiss={() => setShowElement(false)}
                  header={note.titre}
                  subHeader={note.date}
                  message={note.description}
                  buttons={[
                    {
                      text: 'Retour',
                      role: 'cancel',
                      cssClass: 'secondary',
                      handler: () => {
                        console.log('Confirm Cancel');
                      }
                    }
                  ]}
                />
*/
