import React, { useState } from "react";
import { Data } from "./data/Data";
import {
  IonFab,
  IonFabButton,
  IonIcon,
  IonAlert,
  IonPage,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonItem,
  IonTextarea,
  IonInput,
  IonButton,
  IonText,
  IonRouterOutlet,
  IonToggle,
  IonLabel,
  IonDatetime,
} from "@ionic/react";
import { arrowBackOutline } from "ionicons/icons";
import data from './data/data.json';
import events from './data/events.json';
import moment from "moment";



const NewElementButton: React.FC = () => {
  const [createElement, setCreateElement] = useState(false);
  const [titre, setTitre] = useState<string>();
  const [description, setDescription] = useState<string>();
  const [selectedDate, setSelectedDate] = useState<string>('2020-01-01');
  const [rappelDisabled, setRappelDisabled] = useState(true);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Nouvelle note</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Vue liste</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonItem>
          <IonInput
            value={titre}
            placeholder="Entrez le titre ici..."
            onIonChange={(e) => setTitre(e.detail.value!)}
          ></IonInput>
        </IonItem>

        <IonItem>
          <IonTextarea
            value={description}
            onIonChange={(e) => setDescription(e.detail.value!)}
            placeholder="Entrez votre texte ici..."
            id="description"
          ></IonTextarea>
        </IonItem>

        <IonItem>
          <IonToggle color="danger" onClick={() => setRappelDisabled(!rappelDisabled)} />
          <IonLabel>Ajouter un rappel</IonLabel>
        </IonItem>

        <IonItem disabled={rappelDisabled} >
          <IonLabel position="stacked">Date du rappel</IonLabel>
          <IonDatetime displayFormat="D MMMM YYYY HH:mm" min={moment().format("YYYY-MM-DD")} max="2030" value={selectedDate} onIonChange={e => setSelectedDate(e.detail.value!)}></IonDatetime>
        </IonItem>
      
        <IonButton
          color="light"
          onClick={() => {
            console.log("ajouter", Date());
            data.push({
              titre: titre! ,
              description: description! ,
              date: moment().format("YYYY-MM-DD hh:mm"),
              isRappelActivated: !rappelDisabled,
              dateRappel: selectedDate,
              isDeleted: false
            });
            events.push({
              title: titre! ,
              start: moment().format("YYYY-MM-DD hh:mm"),
              end: moment().format("YYYY-MM-DD hh:mm"),
              allDay: true,
              ressources: null
            })
          }}
        >
          Ajouter
        </IonButton>

        <IonFab vertical="bottom" horizontal="start" slot="fixed">
          <IonFabButton color="light" href="/tab1">
            <IonIcon icon={arrowBackOutline} />
          </IonFabButton>
        </IonFab>
      </IonContent>
    </IonPage>
  );
};

export default NewElementButton;
