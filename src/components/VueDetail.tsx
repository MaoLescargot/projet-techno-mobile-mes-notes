import React, { useState } from 'react';
import {Data} from './data/Data';
import { IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent, IonPopover, IonButton, IonFab, IonIcon, IonFabButton, IonFabList, IonAlert } from '@ionic/react';
import { ellipsisHorizontal, ellipsisHorizontalOutline, trashBinOutline, pencilOutline } from 'ionicons/icons';

const VueDetail = (element: Data) => {

  const [showElement, setShowElement] = useState(true);

  return(
    <IonAlert 
      isOpen={showElement}
      onDidDismiss={() => setShowElement(false)}
      header={element.titre}
      subHeader={element.date}
      message={element.description}
      buttons={[
        {
          text: 'Retour',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }
      ]}
    />
  )



  /*
    return(
    <>
      {data ? (
        <>
        <IonFab vertical="top" horizontal="end" slot="fixed" >
        <IonFabButton>
          <IonIcon icon={ellipsisHorizontalOutline} />
        </IonFabButton>
        <IonFabList>
          <IonFabButton>
            <IonIcon icon={pencilOutline} />
          </IonFabButton>
          <IonFabButton>
            <IonIcon icon={trashBinOutline} />
          </IonFabButton>
        </IonFabList>
      </IonFab>
        <IonCard>
            <img src={data.cheminImage} />
            <IonCardHeader>
                <IonCardSubtitle>{data.date}</IonCardSubtitle>
                <IonCardTitle>{data.titre}</IonCardTitle>
            </IonCardHeader>
            <IonCardContent>{data.description}</IonCardContent>
        </IonCard>
      </>
      ) : (
        <div>Chargement en cours</div>
      )}
    </>
    )
  */
};



export default VueDetail;