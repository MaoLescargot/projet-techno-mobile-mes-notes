export interface Data {
    titre: string | undefined;
    description: string | undefined;
    date: string;
    isRappelActivated: boolean;
    dateRappel: string;
    isDeleted: boolean;
  }