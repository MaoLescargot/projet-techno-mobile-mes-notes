import { SQLite } from "@ionic-native/sqlite";
import { Data } from "./Data";
import { isPlatform } from "@ionic/react";
import data from "./data.json";

/*
var inMemoryTasks = data;

const initDBIfNeeded = async () => {
  const db = await SQLite.create({
    name: "data.db",
    location: "default",
  });
  await db.executeSql(
    "CREATE TABLE IF NOT EXISTS taches(identifiant INTEGER PRIMARY KEY AUTOINCREMENT, titre TEXT, description TEXT)",
    []
  );
  return db;
};

export const getTasks = async () => {
  if (!isPlatform("android") && !isPlatform("ios")) {
    // Pas sur mobile, comportement dégradé
    return inMemoryTasks;
  }

  const data = await (await initDBIfNeeded()).executeSql(
    "SELECT * FROM taches",
    []
  );
  const retour: Data[] = [];
  for (let index = 0; index < data.rows.length; index++) {
    const element = data.rows.item(index);
    retour.push(element);
  }
  return retour;
};

export const addTask = async (tache: Data) => {
  if (!isPlatform("android") && !isPlatform("ios")) {
    // Pas sur mobile, comportement dégradé
    inMemoryTasks = [...inMemoryTasks.data, tache];
    return inMemoryTasks;
  }

  await (
    await initDBIfNeeded()
  ).executeSql("INSERT INTO taches(titre,description) VALUES(?,?)", [
    tache.titre,
    tache.description,
  ]);

  return getTasks();
};

*/
