import React, { useState } from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Tab2.css';
import data from '../components/data/data.json';
import Calendrier from '../components/Calendrier';
import events from '../components/data/events.json';



const Tab2: React.FC = () => {
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Mes Notes</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Calendrier</IonTitle>
          </IonToolbar>
        </IonHeader>
        <Calendrier listEvents={events} />
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
